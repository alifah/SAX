package com.example.saxsm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLogin.setOnClickListener {
            var status = if (etUserName.text.toString().equals("Admin") && etPassword.text.toString().equals("SAX"))
                "You are Logged in" else "Login Failed"
            Toast.makeText(this,status,Toast.LENGTH_SHORT).show()
        }
    }
}
